package com.aleksandr.addnotedbtest.presenter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.aleksandr.addnotedbtest.view.ItemNoteAdapter;
import com.aleksandr.addnotedbtest.model.NoteBook;
import com.aleksandr.addnotedbtest.model.NoteContract;
import com.aleksandr.addnotedbtest.model.NoteDbHelperSingleton;

import java.util.ArrayList;

public class Interaction {

    private static Interaction instanceMVP = null;

    public static Interaction getInstanceMVP() {
        if (instanceMVP == null) {
            instanceMVP = new Interaction();
        }
        return instanceMVP;
    }

    private static ContentValues contentValues = new ContentValues();
    private static final String LOG_TAG = "LogCarl";
    private SQLiteDatabase database;

    public void addNote(SQLiteDatabase database) {
        String name = getRandomNote();
        Log.d(LOG_TAG, "Add new note");
        contentValues.put(NoteContract.NoteEntry.COLUMN_TITLE_NOTES, name);
        long rowID = database.insert(NoteContract.NoteEntry.TABLE_NAME, null,
                contentValues);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
    }

    public void readData(SQLiteDatabase database) {
        Log.d(LOG_TAG, "--- Read Data ---");
        Cursor cursor = database.query(NoteContract.NoteEntry.TABLE_NAME, null,
                null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int idColIndex = cursor.getColumnIndex(NoteContract.NoteEntry._ID);
            int nameColIndex = cursor.getColumnIndex(NoteContract.NoteEntry.COLUMN_TITLE_NOTES);

            do {
                Log.d(LOG_TAG, "_ID = " + cursor.getInt(idColIndex) +
                        ", name = " + cursor.getString(nameColIndex));
            } while (cursor.moveToNext());
        } else
            Log.d(LOG_TAG, "No Rows");
        cursor.close();
    }

    public void clearData(SQLiteDatabase database) {
        Log.d(LOG_TAG, "BOOM");
        int clearCount = database.delete(NoteContract.NoteEntry.TABLE_NAME, null,
                null);
        Log.d(LOG_TAG, "deleted rows count = " + clearCount);
    }


    private static String getRandomNote() {
        int random = (int) (Math.random() * 100);
        return "Note # " + random;
    }

    public ArrayList<NoteBook> readNoteBooks(Context context) {
        ArrayList<NoteBook> result = null;
        try {
            open(context);
            result = readNoteBookObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(context);
        }
        return result;
    }

    private ArrayList<NoteBook> readNoteBookObject() {
        ArrayList<NoteBook> noteBooks = new ArrayList<>();
        Cursor cursor = database.query(NoteContract.NoteEntry.TABLE_NAME, null,
                null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                noteBooks.add(getCursorNotebook(cursor));
            } while (cursor.moveToNext());
        }
        return noteBooks;
    }

    private static NoteBook getCursorNotebook(Cursor cursor) {
        NoteBook noteBook = new NoteBook();
        noteBook._id = cursor.getInt(0);
        noteBook.noteTitle = cursor.getString(1);
        return noteBook;
    }

    private void open(Context context) {
        database = NoteDbHelperSingleton.getInstance(context).getReadableDatabase();
    }

    private void close(Context context) {
        NoteDbHelperSingleton.getInstance(context).close();
    }

    public void updateNoteList(ItemNoteAdapter adapterNote, Context context) {
        ArrayList<NoteBook> noteBooks = readNoteBooks(context);
        adapterNote.dataUpdate(noteBooks);
    }
}
