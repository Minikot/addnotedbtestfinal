package com.aleksandr.addnotedbtest.model;

public class NoteBook {
    public int _id;
    public String noteTitle;

    public NoteBook() {
    }

    public String getNoteTitle() {
        return noteTitle;
    }
}
