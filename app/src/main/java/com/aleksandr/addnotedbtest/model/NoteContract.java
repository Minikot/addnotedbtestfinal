package com.aleksandr.addnotedbtest.model;

import android.provider.BaseColumns;

public class NoteContract {

    private NoteContract() {
    }

    public static final class NoteEntry implements BaseColumns {
        public static final String TABLE_NAME = "notes";
        public static final String COLUMN_TITLE_NOTES = "title_notes";
    }
}
