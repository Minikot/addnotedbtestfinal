package com.aleksandr.addnotedbtest.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class NoteDbHelperSingleton extends SQLiteOpenHelper {

    private static NoteDbHelperSingleton mInstance = null;

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "notes.sqlite";


    public static NoteDbHelperSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NoteDbHelperSingleton(context);
        }
        return mInstance;
    }

    public NoteDbHelperSingleton(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + NoteContract.NoteEntry.TABLE_NAME + " (" +
                NoteContract.NoteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NoteContract.NoteEntry.COLUMN_TITLE_NOTES + " TEXT NOT NULL); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + NoteContract.NoteEntry.TABLE_NAME);
        onCreate(db);
    }
}
