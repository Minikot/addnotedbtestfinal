package com.aleksandr.addnotedbtest.view;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.aleksandr.addnotedbtest.presenter.Interaction;
import com.aleksandr.addnotedbtest.R;
import com.aleksandr.addnotedbtest.model.NoteBook;
import com.aleksandr.addnotedbtest.model.NoteDbHelperSingleton;

import java.util.ArrayList;

public class AddNoteActivity extends AppCompatActivity {

    RecyclerView rvNotes;
    private ItemNoteAdapter adapterNote;
    public static Context sApplicationContext = null;
    private NoteDbHelperSingleton dbHelper;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        sApplicationContext = this.getApplicationContext();
        dbHelper = NoteDbHelperSingleton.getInstance(sApplicationContext);

        Button btnAddNote = findViewById(R.id.btn_addNote);
        Button btnReadData = findViewById(R.id.btn_readData);
        Button btnClear = findViewById(R.id.btn_clearData);

        rvNotes = findViewById(R.id.rv_note_list);
        adapterNote = new ItemNoteAdapter(new ArrayList<NoteBook>());
        rvNotes.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        rvNotes.setAdapter(adapterNote);

        /**
         * btnAddNote
         */
        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = dbHelper.getWritableDatabase();
                Interaction.getInstanceMVP().addNote(database);
                dbHelper.close();
                Interaction.getInstanceMVP().updateNoteList(adapterNote, sApplicationContext);
            }
        });

        /**
         * btnReadData
         */
        btnReadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = dbHelper.getWritableDatabase();
                Interaction.getInstanceMVP().readData(database);
            }
        });

        /**
         * btnClearData
         */
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = dbHelper.getWritableDatabase();
                Interaction.getInstanceMVP().clearData(database);
                Interaction.getInstanceMVP().updateNoteList(adapterNote, sApplicationContext);
            }
        });
    }
}
