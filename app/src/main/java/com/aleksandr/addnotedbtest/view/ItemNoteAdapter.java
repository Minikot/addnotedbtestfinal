package com.aleksandr.addnotedbtest.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleksandr.addnotedbtest.R;
import com.aleksandr.addnotedbtest.model.NoteBook;

import java.util.ArrayList;

public class ItemNoteAdapter extends RecyclerView.Adapter<ItemNoteAdapter.ViewHolder> {

    private ArrayList<NoteBook> noteBooks;

    public ItemNoteAdapter(ArrayList<NoteBook> noteBooks) {
        this.noteBooks = noteBooks;
        notifyDataSetChanged();
    }

    public void dataUpdate(ArrayList<NoteBook> noteBooks) {
        if (noteBooks != null) {
            this.noteBooks = noteBooks;
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_note, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.bindTo(noteBooks.get(holder.getAdapterPosition()));
        }

    @Override
    public int getItemCount() {
        return noteBooks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvNoteTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNoteTitle = itemView.findViewById(R.id.tv_title_note_adapter);
        }

        private void bindTo(final NoteBook noteBook) {
            tvNoteTitle.setText(noteBook.getNoteTitle());
        }
    }
}
